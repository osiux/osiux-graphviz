#!/bin/bash

# This script comes with ABSOLUTELY NO WARRANTY, use at own risk
# Copyright (C) 2021 Osiris Alejandro Gomez <osiux@osiux.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

[[ -n "$HEADER"  ]] || HEADER='dot-header.org'
[[ -n "$FOOTER"  ]] || FOOTER='dot-footer.org'
[[ -n "$LICENSE" ]] || LICENSE='🄯 CC-BY-SA OSiUX.com'
[[ -n "$DOT_URL" ]] || DOT_URL='https://gitlab.com/osiux/osiux-graphviz/-/raw/develop'

get_label()
{
   grep -w graph.*label "$1"   \
     | grep -v '// '           \
     | grep -Eo 'label=\".*\"' \
     | head -1                 \
     | cut -d '=' -f2          \
     | tr -d '"'               \
     | sed "s/$LICENSE//g"     \
     | sed 's/ \+$//g'
}

[[ -e "$HEADER" ]] && cat "$HEADER"

find . -type f -iname '*.dot' \
  | cut -d/ -f2-              \
  | sort -nr                  \
  | while read -r DOT
do

  PNG="$(basename "$DOT" .dot).png"

  TITLE="$(get_label "$DOT")"
  [[ -z "$TITLE" ]] && TITLE="$(basename "$DOT" .dot | tr -d '-')"

  printf "\\n** [[%s][%s]]\\n\\n"          "$DOT_URL/$DOT" "$TITLE"
  printf "\\n   [[file:img/%s][file:tmb/%s]]\\n\\n" "$PNG" "$PNG"

done

[[ -e "$FOOTER" ]] && cat "$FOOTER"
