# `osiux-graphviz`

Gallery of GraphViz diagrams in `dot` language.

## Install

### Manual

Clone the repository:

```bash

  cd /opt
  git clone https://gitlab.com/osiux/osiux-graphviz
  cd osiux-graphviz
  make all

```

## License

_GNU General Public License, GPLv3._

## Author Information

This repo was created in 2023 by
 [Osiris Alejandro Gomez](https://osiux.com/), worker cooperative of
 [gcoop Cooperativa de Software Libre](https://www.gcoop.coop/).
