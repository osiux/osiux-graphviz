SHELL:=/bin/bash

SITE ?= osiux-graphviz
TGZ  ?= $(SITE).tar.gz

all: png image dot2org publish tar_gz

png:
	mkdir -p img
	for i in dot/*.dot;do ./dot2png "$$i" > "img/$$(basename "$$i" .dot).png";done

image:
	mkdir -p tmb
	./tmb.sh

dot2org:
	./dot.sh > dot.org

publish:
	./publish.sh

tar_gz:
	tar czf public/$(TGZ) dot.org img/*.png tmb/*.png && md5sum public/$(TGZ) | tee public/$(TGZ).md5
